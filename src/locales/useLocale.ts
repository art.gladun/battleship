import { useState } from "react";

import battleshipEn from "./en/battleship.json";

const resources = {
  en: {
    battleship: battleshipEn,
  },
}


type LocaleResources = typeof resources;
type Locale = keyof LocaleResources;
type LocaleScope = keyof LocaleResources["en"];
type LangNameSpace = keyof LocaleResources["en"]["battleship"]

export const useLocale = (scope: LocaleScope) => {
  const [locale, setLocale] = useState<Locale>("en");

  const t = (key: LangNameSpace) => {
    return resources[locale][scope][key];
  }

  return {
    t,
    setLocale,
  }
}
