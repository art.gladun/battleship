export type ShipType =
  | "carrier"
  | "battleship"
  | "cruiser"
  | "submarine"
  | "destroyer";

export interface ShipConfig {
  size: number;
}

export type ShipsConfig = Record<ShipType, ShipConfig>;
export type PositionToLayoutMap = Record<number, number>;

export interface ShipPosition {
  ship: ShipType;
  positions: Array<Array<number>>;
}

export interface Shots {
  [xy: string]: boolean;
}

export type State = 'initialized' | 'pending' | 'over';

export interface GameState {
  shots: Shots;
  shipPositions: Array<ShipPosition>;
  state: State;

  readonly positionToShipMap: PositionToLayoutMap;
}
