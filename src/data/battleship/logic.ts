import {
  ShipsConfig,
  GameState,
  ShipPosition,
  PositionToLayoutMap,
  Shots,
  ShipType
} from "./types";

export const BOARD_SIZE = 10;

export const getFlatPosition = ({ x, y }: { x: number; y: number }) => {
  return y * BOARD_SIZE + x;
};

export const shipTypes: ShipsConfig = {
  destroyer: { size: 5 },
  battleship: { size: 4 },
  cruiser: { size: 3 },
  submarine: { size: 3 },
  carrier: { size: 2 },
};

// definition, what and how many ships to generate
const TO_GENERATE: ShipType[] = ['carrier', 'carrier', 'battleship', 'cruiser', 'submarine', 'destroyer'];

type Direction = "V" | "H";

const canPlaceShip = (x: number, y: number, size: number, direction: Direction, usedPostions: Set<number>) => {
  for (let i = 0; i < size; i++) {
    const newX = direction === 'H' ? x + i : x;
    const newY = direction === 'H' ? y : y + 1;

    const flatPosition = getFlatPosition({ x: newX, y: newY });
    if (usedPostions.has(flatPosition) || flatPosition < 0 || flatPosition >= BOARD_SIZE * BOARD_SIZE) {
      return false;
    }
  }

  return true;
}

export const getShips = () => {

  const order = TO_GENERATE.sort((a: ShipType, b: ShipType) => shipTypes[b].size - shipTypes[a].size)

  const usedPositions = new Set<number>();
  const shipPositions = [];

  for (const shipType of order) {
    const size = shipTypes[shipType].size;
    const directions: Direction[] = ["V", "H"];

    let shipPlaced = false;
    while (!shipPlaced) {
      const x = Math.floor(Math.random() * BOARD_SIZE);
      const y = Math.floor(Math.random() * BOARD_SIZE);
      const direction = directions[Math.floor(Math.random() * directions.length)];

      if (canPlaceShip(x, y, size, direction, usedPositions)) {
        shipPlaced = true;

        shipPositions.push({
          ship: shipType,
          positions: Array.from({ length: size }, (_, i) => {
            const newX = direction === 'H' ? x + i : x;
            const newY = direction === 'H' ? y : y + 1;

            usedPositions.add(getFlatPosition({ x: newX, y: newY }))
            return direction === 'H' ? [x + i, y] : [x, y + i]
          })
        })
      }
    }
  }


  // TODO generate ship and their postion based on TO_GENERATE array of ships
  // sorted by length (it will look beter)
  // const shipPositions: Array<ShipPosition> = [
  //   {
  //     ship: "destroyer",
  //     positions: [
  //       [2, 9],
  //       [3, 9],
  //       [4, 9],
  //       [5, 9],
  //       [6, 9],
  //     ],
  //   },
  //   {
  //     ship: "destroyer",
  //     positions: [
  //       [9, 5],
  //       [9, 6],
  //       [9, 7],
  //       [9, 8],
  //       [9, 9],
  //     ],
  //   },
  //   {
  //     ship: "battleship",
  //     positions: [
  //       [5, 2],
  //       [5, 3],
  //       [5, 4],
  //       [5, 5],
  //     ],
  //   },
  //   {
  //     ship: "cruiser",
  //     positions: [
  //       [8, 1],
  //       [8, 2],
  //       [8, 3],
  //     ],
  //   },
  //   {
  //     ship: "submarine",
  //     positions: [
  //       [3, 0],
  //       [3, 1],
  //       [3, 2],
  //     ],
  //   },
  //   {
  //     ship: "carrier",
  //     positions: [
  //       [0, 0],
  //       [1, 0],
  //     ],
  //   },
  // ];

  const positionToShipMap = shipPositions.reduce(
    (map: PositionToLayoutMap, { positions }: ShipPosition, index: number) => {
      positions.forEach(([x, y]) => (map[getFlatPosition({ x, y })] = index));
      return map;
    },
    {},
  );

  return {
    shots: {},
    shipPositions,
    positionToShipMap,
  };
};

export const createInitilizedGameState = (): GameState => {
  return {
    shots: {},
    shipPositions: [],
    positionToShipMap: {},
    state: 'initialized',
  }
}

export const createPendingGameState = (): GameState => {
  return {
    ...getShips(),
    state: 'pending',
  }
}

export const countShipHits = (
  positions: ShipPosition["positions"],
  shots: Shots,
) => {
  return positions.reduce((acc, [x, y]) => {
    const shotPosition = getFlatPosition({ x, y });
    return (acc += shots[shotPosition] ? 1 : 0);
  }, 0);
};


export const isGameOver = (gameState: GameState) => {
  let gameIsOver = true;
  for (let i = 0; i < gameState.shipPositions.length; i++) {
    const { positions } = gameState.shipPositions[i];
    if (
      positions.some(
        ([x, y]: Array<number>) =>
          !gameState.shots[getFlatPosition({ x, y })],
      )
    ) {
      gameIsOver = false;
    }
  }

  return gameIsOver;
}
