import classNames from "classnames";

import { PropsWithAs, forwardRefWithAs } from "utils/withAs";

const variantPrimary = classNames(
  "bg-yellow-500",
  "hover:bg-yellow-600",
  "border-yellow-500",
  "hover:border-yellow-600",
  "tet-slate-800",
);

const baseStyles = classNames("inline-block", "p-2");

type ButtonVariatns = "primary" | "secondary";

const variantDict: Record<ButtonVariatns, string> = {
  primary: variantPrimary,
  secondary: "",
};

interface ButtonComponentProps {
  variant?: ButtonVariatns;
}

export const ButtonComponent = (
  props: PropsWithAs<ButtonComponentProps, "button">,
  forwardRef: React.Ref<HTMLButtonElement>,
) => {
  const {
    as: Component = "button",
    variant = "primary",
    className,
    children,
    ...rest
  } = props;

  const classes = classNames(baseStyles, variantDict[variant], className);

  return (
    <Component className={classes} ref={forwardRef} {...rest}>
      {children}
    </Component>
  );
};

export type ButtonProps = PropsWithAs<ButtonComponentProps, "button" | "a">;
export const Button = forwardRefWithAs<ButtonComponentProps, "button" | "a">(
  ButtonComponent,
);
