import { createIcon } from "./createIcon";

import { ReactComponent as CircleSvg } from "./svg/circle.svg";
import { ReactComponent as CloseSvg } from "./svg/close.svg";

export const CircleIcon = createIcon(CircleSvg);
export const CloseIcon = createIcon(CloseSvg);
