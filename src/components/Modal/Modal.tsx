import classNames from "classnames";
import { PropsWithChildren } from "react"

const DialogBackdrop: React.FC<PropsWithChildren> = ({ children }) => {
  const classes = classNames('fixed', 'inset-0', 'w-full', 'h-full', 'bg-black/40');

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

const DialogContainer: React.FC<PropsWithChildren> = ({ children }) => {
  const classes = classNames('w-full', 'max-w-[600px]', 'mx-auto',  "p-4");

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

export const ModalContent: React.FC<PropsWithChildren> = ({ children }) => {
  const classes = classNames('bg-white', "p-2");

  return <div className={classes}>
    {children}
  </div>
}

export interface ModalProps extends PropsWithChildren {
  visible: boolean;
}

export const Modal: React.FC<ModalProps> = ({ children, visible }) => {

  if (!visible) {
    return null;
  }

  return (
    <DialogBackdrop>
      <DialogContainer>
        {children}
      </DialogContainer>
    </DialogBackdrop>
  )
}
