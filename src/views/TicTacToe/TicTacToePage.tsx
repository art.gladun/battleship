import { PageContainer, PageContent } from "components/Page";

export const TicTacToePage = () => {
  return (
    <PageContent>
      <PageContainer>TicTacToe game!!</PageContainer>
    </PageContent>
  );
};
