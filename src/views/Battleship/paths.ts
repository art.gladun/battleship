const ROOT_PATH = "/battleship";

export const ROOT = {
  PATH: ROOT_PATH,
  toPath: () => ROOT_PATH,
};
