import React from "react";
import classNames from "classnames";

import { MissShot, HitShot } from "../Shots";

interface Props {
  shot?: boolean;
  hit?: boolean;

  position: number;
}

const tdClasses = classNames(
  "relative",
  "border-2",
  "border-solid",
  "border-neutral-500",
  `w-[10%]`,
);

export const BoardCell: React.FC<Props> = React.memo(
  ({ shot, hit, position }) => {
    const squareClasses = classNames(
      "flex",
      "flex-col",
      "justify-center",
      "items-center",
      "aspect-square",
      !shot ? "cursor-pointer" : "",
    );

    return (
      <td className={tdClasses}>
        <div className={squareClasses} data-position={position}>
          {shot && !hit ? <MissShot /> : null}
          {shot && hit ? <HitShot /> : null}
        </div>
      </td>
    );
  },
);
