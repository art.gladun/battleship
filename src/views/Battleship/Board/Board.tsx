import classNames from "classnames";

import {
  BOARD_SIZE,
  Shots,
  PositionToLayoutMap,
  getFlatPosition,
} from "data/battleship";

import { BoardCell } from "./BoardCell";

interface Props {
  shots: Shots;
  positionToShipMap: PositionToLayoutMap;
  onShot: (positionKey: number) => void;
}

const tableStyles = classNames(
  "border-solid",
  "border-4",
  "border-yellow-500",
  "w-full",
);

export const Board: React.FC<Props> = ({
  shots,
  positionToShipMap,
  onShot,
}) => {
  const handleClick = (event: React.MouseEvent<HTMLTableElement>) => {
    const position = (event.target as HTMLTableElement).dataset.position;

    if (typeof position === "string") {
      onShot(Number(position));
    }
  };

  const trs = Array.from({ length: BOARD_SIZE }, (_, y) => {
    return (
      <tr key={y}>
        {Array.from({ length: BOARD_SIZE }, (_, x) => {
          const position = getFlatPosition({ x, y });
          const shot = shots[position];
          const hit = typeof positionToShipMap[position] === "number";

          return (
            <BoardCell
              key={position}
              data-cell={position}
              shot={shot}
              hit={hit}
              position={position}
            />
          );
        })}
      </tr>
    );
  });

  return (
    <table className={tableStyles} onClick={handleClick}>
      <tbody>{trs}</tbody>
    </table>
  );
};
