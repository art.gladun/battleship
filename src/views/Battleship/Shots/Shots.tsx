import hitImg from "./assets/hit-red.png";
import missImg from "./assets/miss.png";

export const HitShot = ({ disabled }: { disabled?: boolean }) => {
  return (
    <img className={disabled ? "opacity-50" : ""} src={hitImg} alt="Hit" />
  );
};

export const MissShot = () => {
  return <img src={missImg} alt="Miss" />;
};
