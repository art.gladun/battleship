import {
  ShipPosition,
  ShipType,
  shipTypes,
  Shots,
  countShipHits,
} from "data/battleship";

import { Battleship, Carrier, Cruiser, Destroyer, Submarine } from "./Ships";
import { HitShot } from "./Shots";
import { PropsWithChildren } from "react";

interface Props {
  positions: Array<ShipPosition>;
  shots: Shots;
}

const ShipContainer: React.FC<PropsWithChildren> = ({ children }) => {
  return <div>{children}</div>;
};

const StatsLineContainer: React.FC<PropsWithChildren> = ({ children }) => {
  return <div className="flex space-x-2">{children}</div>;
};

const HitsContainer: React.FC<PropsWithChildren> = ({ children }) => {
  return <div className="grid grid-cols-5 gap-px items-center">{children}</div>;
};

const shipComponents: Record<ShipType, React.ReactNode> = {
  carrier: <Carrier />,
  battleship: <Battleship />,
  cruiser: <Cruiser />,
  submarine: <Submarine />,
  destroyer: <Destroyer />,
};

export const Stats: React.FC<Props> = ({ positions, shots }) => {
  return (
    <div className="flex flex-col justify-center space-y-2 w-[70%] xs:w-full">
      {positions.map(({ ship, positions }: ShipPosition, index: number) => {
        const ShipComponent = shipComponents[ship];
        const hits = countShipHits(positions, shots);
        const stats = Array.from({ length: shipTypes[ship].size }, (_, i) => {
          return <HitShot key={i} disabled={i >= hits} />;
        });

        return (
          <StatsLineContainer key={index}>
            <ShipContainer>{ShipComponent}</ShipContainer>
            <HitsContainer>{stats}</HitsContainer>
          </StatsLineContainer>
        );
      })}
    </div>
  );
};
