import classNames from "classnames";
import { PropsWithChildren } from "react";

import { Modal, ModalProps, ModalContent } from "components/Modal";
import { Button } from "components/Button";

import { Board } from "./Board";
import { Stats } from "./Stats";

import { useLocale } from "locales";

import { useGame } from "./useGame";

const classes = classNames(
  "flex-1",
  "grid",
  "grid-rows-[auto_1fr]",
  "xs:grid-rows-1",
  "xs:grid-cols-[minmax(auto,_0.5fr)_1fr]",
  "gap-2",
  "sm:gap-4",
  "mx-auto",
  "w-full",
);

interface InitializedModalProps extends ModalProps {
  onClick: () => void;
}

const InitializedModal: React.FC<InitializedModalProps> = ({
  visible,
  onClick,
}) => {
  const { t } = useLocale("battleship");

  return (
    <Modal visible={visible}>
      <ModalContent>
        <div>{t("WELCOME_MESSAGE")}</div>
        <Button onClick={onClick}>Start</Button>
      </ModalContent>
    </Modal>
  );
};

interface OverModalProps extends ModalProps {
  onClick: () => void;
}

const OverModal: React.FC<OverModalProps> = ({ visible, onClick }) => {
  const { t } = useLocale("battleship");

  return (
    <Modal visible={visible}>
      <ModalContent>
        <div>{t("GAME_OVER_MESSAGE")}</div>
        <Button onClick={onClick}>Start</Button>
      </ModalContent>
    </Modal>
  );
};

export const BattleshipGame: React.FC<PropsWithChildren> = () => {
  const { state, actions } = useGame();

  return (
    <div className={classes}>
      <Stats positions={state.shipPositions} shots={state.shots} />
      <Board
        shots={state.shots}
        positionToShipMap={state.positionToShipMap}
        onShot={actions.handleShot}
      />
      <InitializedModal
        visible={state.state === "initialized"}
        onClick={actions.handleStartGame}
      />
      <OverModal
        visible={state.state === "over"}
        onClick={actions.handleRestartGame}
      />
    </div>
  );
};
