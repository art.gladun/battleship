import { PageContainer, PageContent } from "components/Page";

import { BattleshipGame } from "./BattleshipGame";

export const BattleshipPage = () => {
  return (
    <PageContainer>
      <PageContent>
        <BattleshipGame />
      </PageContent>
    </PageContainer>
  );
};
