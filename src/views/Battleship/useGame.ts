import { useState, useCallback } from "react";

import {
  createInitilizedGameState,
  createPendingGameState,
  GameState,
  isGameOver,
} from "data/battleship";

export const useGame = () => {
  const [gameState, setGameState] = useState<GameState>(createInitilizedGameState());

  const handleShot = useCallback(
    (position: number) => {
      const { shots } = gameState;

      if (shots[position]) {
        return;
      }

      const newGameState = {
        ...gameState,
        shots: {
          ...shots,
          [position]: true,
        },
      };

      if (isGameOver(newGameState)) {
        newGameState.state = 'over';
      }

      setGameState(newGameState);
    },
    [gameState],
  );

  const handleRestartGame = () => {
    setGameState(createPendingGameState());
  }

  const handleStartGame = () => {
    setGameState(createPendingGameState());
  }

  return {
    state: gameState,
    actions: {
      handleShot,
      handleRestartGame,
      handleStartGame,
    },
  };
};
