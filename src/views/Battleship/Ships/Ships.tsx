import battleshipImg from "./assets/battleship.png";
import carrierpImg from "./assets/carrier.png";
import cruiserImg from "./assets/cruiser.png";
import destroyerImg from "./assets/destroyer.png";
import submarineImg from "./assets/submarine.png";

export const Battleship = () => {
  return <img src={battleshipImg} alt="Battleship" />;
};

export const Carrier = () => {
  return <img src={carrierpImg} alt="Carrier" />;
};

export const Cruiser = () => {
  return <img src={cruiserImg} alt="Cruiser" />;
};

export const Destroyer = () => {
  return <img src={destroyerImg} alt="Destroyer" />;
};

export const Submarine = () => {
  return <img src={submarineImg} alt="Submarine" />;
};
