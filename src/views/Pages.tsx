import React from "react";
import { Route, Routes, useNavigate } from "react-router-dom";

import { PageLayout } from "components/Page";

import { BattleshipPage } from "views/Battleship";

import { PATHS } from "./paths";
import { useEffect } from "react";

const Redirect = () => {
  const navigate = useNavigate();

  useEffect(() => {
    navigate(PATHS.BATTLESHIP.ROOT.toPath());
  }, [navigate]);

  return null;
};

export const Pages = () => {
  return (
    <PageLayout>
      <Routes>
        <Route path={PATHS.BATTLESHIP.ROOT.PATH} element={<BattleshipPage />} />
        <Route path="*" element={<Redirect />} />
      </Routes>
    </PageLayout>
  );
};
